'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('framedPotatoApp'));

  var MainCtrl,
    scope,
    $httpBackend,
    location;

  // Initialize the controller and a mock scope
  beforeEach(inject(function (_$httpBackend_, $controller, $rootScope, $location, tagQuery) {
    var numFetched = 0;
    $httpBackend = _$httpBackend_;
    $httpBackend.when('JSONP','http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=JSON_CALLBACK&tagmode=all&tags=potato')
      .respond({items : ['photoData1', 'photoData2', 'photoData3']});
    $httpBackend.when('JSONP',/^http:\/\/api\.flickr\.com\/services\/feeds\/photos_public\.gne\?format=json&jsoncallback=JSON_CALLBACK&tagmode=all&tags=fetched-random-.*$/)
        .respond({items : ['random1', 'random2', 'random3']});
    $httpBackend.when('GET', '/api/randomWord')
        .respond(function() {
          numFetched += 1;
          return [200, 'fetched-random-' + numFetched, {}];
        });
    tagQuery.set('defaultTag');
    scope = $rootScope.$new();
    location = $location;
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should load photos of potatos by default', function () {
    expect(scope.photoData.items).toBeUndefined();
    $httpBackend.flush();
    expect(scope.photoData.items.length).toBe(3);
  });

  it('should load the last filter tag set in the tag service', function () {
    expect(scope.tagSearch.q).toBe('defaultTag');
  });

  it('should load the tag used to make the last fetch from Flickr', function() {
    expect(scope.queryString).toBe('potato');
  });

  it('should have a method \'goTo()\' to change the path', function() {
    scope.goTo('testLoc');
    expect(location.path()).toBe('/testLoc');
  });

  it('should have a method getRandomPhotos, which should set the query string to a new value', function() {
    $httpBackend.flush();
    scope.getRandomPhotos();
    $httpBackend.flush();
    expect(scope.queryString).toBe('fetched-random-1');
  });

  it('should have a method getRandomPhotos, which should set the photo data to new photos', function() {
    $httpBackend.flush();
    scope.getRandomPhotos();
    $httpBackend.flush();
    expect(scope.photoData.items[0]).toBe('random1');
  });

  it('should have a method getRandomPhotos, which should clear the tag search', function() {
    scope.tagSearch.q = 'a string';
    scope.getRandomPhotos();
    expect(scope.tagSearch.q).toBe('');
  });
});
