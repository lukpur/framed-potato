'use strict';

describe('service:fpUtils', function() {
  beforeEach(module('framedPotatoApp'));
  var service;
  beforeEach(inject(function (_$httpBackend_, fpUtils) {
    service = fpUtils;
  }));

  it('should truncate a word if that word is longer than the given threshold, and append and ellipsis', function() {
    expect(service.truncate('My Long String', 10)).toBe('My Long S…');
  });

  it('shouldn\' truncate a word if that word is shorter than the given threshold', function() {
    expect(service.truncate('My Long String', 20)).toBe('My Long String');
  });

  it('shouldn\' truncate a word if that word\' length is equal to the given threshold', function() {
    expect(service.truncate('My Long String', 14)).toBe('My Long String');
  });

});