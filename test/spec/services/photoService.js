'use strict';

describe('service:photoService', function() {
  beforeEach(module('framedPotatoApp'));
  var service,
      $httpBackend;
  beforeEach(inject(function (_$httpBackend_, photoService) {
    service = photoService;
    $httpBackend = _$httpBackend_;
    // The below request will be made in all instantiations of the
    // photo service, so define it in beforeEach
    $httpBackend.expectJSONP('http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=JSON_CALLBACK&tagmode=all&tags=potato')
        .respond({response: 'fetched-potato'});
  }));

  it('should get photos containing the tag potato by default', function() {
    var result;
    result = service.get();
    $httpBackend.flush();
    expect(result.response).toBe('fetched-potato');
  });

  it('should get photos containing the input tag', function() {
    var result;
    $httpBackend.expectJSONP('http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=JSON_CALLBACK&tagmode=all&tags=newTag')
        .respond({response: 'fetched-new-tag'});
    result = service.load('newTag');
    $httpBackend.flush();
    expect(result.response).toBe('fetched-new-tag');
  });

  it('should return the tag potato by default', function() {
    expect(service.tag()).toBe('potato');
  });

  it('should return the tag of the last fetched query', function () {
    $httpBackend.expectJSONP('http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=JSON_CALLBACK&tagmode=all&tags=newTag')
        .respond({response: 'fetched-new-tag'});
    service.load('newTag');
    $httpBackend.flush();
    expect(service.tag()).toBe('newTag');
  });
});