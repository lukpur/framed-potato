'use strict';

describe('service:goHome', function() {
  beforeEach(module('framedPotatoApp'));
  var service,
      location;
  beforeEach(inject(function ($location, goHome) {
    location = $location;
    service = goHome;
  }));

  it('should make the location \'/\'', function() {
    service();
    expect(location.path()).toBe('/');
  });

});