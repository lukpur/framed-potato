'use strict';

describe('service:RandomTag', function() {
  beforeEach(module('framedPotatoApp'));
  var service,
      $httpBackend;
  beforeEach(inject(function (_$httpBackend_, RandomTag) {
    var numFetched = 0;
    service = RandomTag;
    $httpBackend = _$httpBackend_;
    // The below request will be made in all instantiations of the
    // photo service, so define it in beforeEach
    $httpBackend.when('GET', '/api/randomWord')
        .respond(function() {
          numFetched += 1;
          return [200, 'fetched-' + numFetched, {}];
        });
    $httpBackend.flush(); // fetch initial word
  }));

  it('should be initialised with a random word', function() {
    var result;
    result = service.get();
    expect(result).toBe('fetched-1');
  });

  it('should not fetch a new word from API when get() is invoked', function() {
    var result1,
        result2;
    result1 = service.get();
    // ensure get doesn't invoke a fetch
    expect(function() {
      $httpBackend.flush();
    }).toThrow(new Error('No pending request to flush !'));

    result2 = service.get();
    expect(result1).toBe('fetched-1');
    expect(result1).toBe(result2);
  });

  it('should get a new word when consume() is called', function() {
    var result1,
        result2;
    result1 = service.consume();
    $httpBackend.flush();
    result2 = service.get();
    expect(result1).toBe('fetched-1');
    expect(result2).toBe('fetched-2');
  });

});