'use strict';

var http = require('http');

var randomWordOpts = {
  host: 'http://randomword.setgetgo.com',
  port: 80,
  path: '/get.php',
  method: 'GET'
};

exports.randomWord = function(req, res) {
  http.get('http://randomword.setgetgo.com/get.php', function(r) {
    r.setEncoding('utf8');
    r.on('data', function(d) {
      res.end(d.replace('\n', '').replace('\r', ''));
    });
  });
};

