'use strict';

angular.module('framedPotatoApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute'
])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'partials/main',
        controller: 'MainCtrl'
      })
      .when('/:hashKey', {
        templateUrl: 'partials/detail',
        controller: 'DetailCtrl'
      })
      .otherwise({
          redirectTo: '/'
        });
      
    $locationProvider.html5Mode(true);
  });