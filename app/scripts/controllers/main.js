'use strict';

angular.module('framedPotatoApp')

    // SERVICES
    .service('photoService', function($resource) {
      var me;
      this.resource = $resource('http://api.flickr.com/services/feeds/photos_public.gne', {
          format: 'json',
          tagmode: 'all',
          jsoncallback: 'JSON_CALLBACK'
        }, {
          'load' : {
            'method' : 'JSONP'
          }
        });
      // initialize photo service with photos of potatos
      this.tag = 'potato';
      this.currentData = this.resource.load({tags: this.tag});
      me = this;
      return {
        load : function (tag) {
          if (tag === me.tag) {
            return me.currentData;
          }
          me.currentData = me.resource.load({tags: tag});
          me.tag = tag;
          return me.currentData;
        },
        get : function() {
          return me.currentData;
        },
        tag : function() {
          return me.tag;
        }
      };
    })
    .service('fpUtils', function() {
      return {
        // Utility function to trim a string and append an ellipsis if it is beyond a limit
        truncate : function(str, limitTo) {
          if (str.length > limitTo) {
            return str.substring(0, limitTo-1) + '…';
          }
          return str;
        }
      };
    })
    .service('RandomTag', function($resource) {
      var me;
      this.resource = $resource('/api/randomWord',{}, {
        get : {
          method: 'GET',
          isArray: false,
          transformResponse: function(data) {
            return {
              word: data.replace('\n', '').replace('\r','')
            };
          }
        }
      });
      this.currentWord = this.resource.get();
      me = this;
      return {
        consume: function() {
          var oldWord = me.currentWord.word;
          me.currentWord = me.resource.get();
          return oldWord;
        },
        get: function() {
          return me.currentWord.word;
        }
      };
    })
    .service('tagQuery', function() {
      // implement tag search binding as an app-wide service for communication
      // between main page and detail page
      this.q = '';
      this.set = function (newQ) {
        this.q = newQ;
      };
    })
    .service('goHome', function($location) {
      return function() {
        $location.path('/');
      };
    })
    .service('viewPhotosWithTag', function($location, tagQuery) {
      return function(t) {
        tagQuery.set(t);
        $location.path('/');
      };
    })

    // CONTROLLERS
  .controller('MainCtrl', function ($scope, $http, photoService, $location, RandomTag, tagQuery) {
    $scope.tagSearch = tagQuery;
    $scope.photoData = photoService.get();
    $scope.queryString = photoService.tag();
    $scope.goTo = function(loc) {
      $location.path(loc);
    };

    $scope.getRandomPhotos = function() {
      $scope.queryString = RandomTag.consume();
      $scope.photoData = photoService.load($scope.queryString);
      $scope.tagSearch.q = '';
    };

    $scope.getPhotosForTag = function() {
      if ($scope.tagSearch.q) {
        $scope.queryString = $scope.tagSearch.q;
        $scope.photoData = photoService.load($scope.tagSearch.q);
      }
    };
  })
  .controller('DetailCtrl', function ($scope, $routeParams, $filter, photoService, $location, tagQuery, goHome) {
    $scope.filtered = $filter('filter')(photoService.get().items,
        function(item) {
          return item.$$hashKey === $routeParams.hashKey;
        });
    $scope.goHome = goHome;
  })

    // DIRECTIVES
  .directive('fpDescription', function() {
    return {
      restrict : 'E',
      scope : {
        description: '='
      },
      link: function(scope) {
        // Remove the first two paragraphs from the description, as they contain
        // generic picture and title data that we have already displayed elsewhere
        scope.cleanedHtml = scope.description.replace(/<p>.*?<\/p>.*?<p>.*?<\/p>/, '');
      },
      template: '<div ng-bind-html="cleanedHtml" class="fp-description"></div>',
      replace: true
    };
  })
  .directive('fpTags', function(viewPhotosWithTag) {
      var tagQuery = viewPhotosWithTag;
      return {
        restrict: 'E',
        scope: {
          tags: '='
        },
        link: function(scope) {
          scope.tagArr = scope.tags.split(' ');
          scope.viewPhotosWithTag = tagQuery;
        },
        template: '<span class="fp-label">Tags: </span><span class="fp-tag" ng-repeat="tag in tagArr" ng-click="viewPhotosWithTag(tag)">{{tag}} </span>'
      };
    })

    // FILTERS
  .filter('ordinalSuffix', function() {
      return function(input) {
        var suffix,
            tokens = input.split(' '),
            ordinals = tokens[0],
            lastNumeral = ordinals[ordinals.length-1];
        if (lastNumeral === '1') {
          suffix = 'st';
        } else if (lastNumeral === '2') {
          suffix = 'nd';
        } else if (lastNumeral === '3') {
          suffix = 'rd';
        } else {
          suffix = 'th';
        }
        tokens[0] = tokens[0] + suffix;
        return tokens.join(' ');
      };
    })
  .filter('formatAuthor', function(fpUtils) {
    return function(input, limitTo) {
      var trimmed = input.replace('nobody@flickr.com (', '').replace(/\)$/, '');
      return fpUtils.truncate(trimmed, limitTo);
    };
  })
  .filter('truncate', function(fpUtils) {
    return function (input, limitTo) {
      return fpUtils.truncate(input, limitTo);
    };
  });


