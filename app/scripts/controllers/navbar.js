'use strict';

angular.module('framedPotatoApp')
  .config(function($httpProvider) {
      $httpProvider.defaults.useXDomain = true;
    })
  .controller('NavbarCtrl', function ($scope, $location) {
    // Not needed for this app
    $scope.menu = [];
    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });
